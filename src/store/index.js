import { createStore } from "vuex";
import * as data from "../../db.json";
export default createStore({
  state: {
    data: data.tableData,
  },
  mutations: {},
  actions: {},
  modules: {},
});
